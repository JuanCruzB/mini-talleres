# -*- coding: utf-8 -*-
"""
Created on Mon Nov 15 16:48:02 2021

@author: Administrador
"""
## Mini Taller 3

import random
import time
import matplotlib.pyplot as plt
import numpy as np
import sys
sys.getrecursionlimit() #devuelve el numero limite de llamadas recursivas
#en mi caso era 1000
sys.setrecursionlimit(1000000000) 

#1)

def posmaximo(a):
    """
    Toma una lista  de números y devuelve el elemento de mayor valor    
    """
    i=0
    x=a[0]
    p=0
    while len(a)>i:
       if a[i]>x:
           x=a[i]
           p=i
       i=i+1
    return p

lista1=[6,2,4,9,2,1]

def upSortSlice(a):
    if len(a)==1:
        return [a[0]]
    elif len(a)==2:
        if a[0]<a[1]:
            return [a[0], a[1]]
        else:
            return [a[1], a[0]]
    else:
        m=posmaximo(a)
        a[m], a[len(a)-1] = a[len(a)-1], a[m]
        return upSortSlice(a[:-1]) + [a[len(a)-1]]
            

upSortSlice(lista1)

#2)

def upSortIndex(a):
    actual=len(a)-1
    while actual>1:
        m=posmaximo(a[:actual+1])
        a[m], a[actual] = a[actual], a[m]
        actual-=1
    return a

lista1=[6,2,4,9,2,1]

upSortIndex(lista1)

#3)
def bubbleSort(a):
    for i in range(0, len(a)):
        for j in range(0, len(a)-1):
            if a[j]>a[j+1]:
                a[j], a[j+1] = a[j+1], a[j]
    return a

bubbleSort(lista1)

#4)
def unirProlijo (a,b):
    listaprolija=[]
    i=0
    j=0
    while i<(len(a)) and j<(len(b)):
        if a[i]<=b[j]:
            listaprolija.append(a[i])
            i=i+1
        elif a[i]>b[j]:
            listaprolija.append(b[j])
            j=j+1
    while j<len(b):
        listaprolija.append(b[j])
        j=j+1       
    while i<len(a):
        listaprolija.append(a[i])
        i=i+1     
    return listaprolija

def mergeSortSlices(a):
    if len(a)==1:
        return [a[0]]
    elif len(a)==2:
        if a[0]<a[1]:
            return [a[0], a[1]]
        else:
            return [a[1], a[0]]
    else:
        while len(a)>2:
            b=len(a)//2
            c=mergeSortSlices(a[:b])
            d=mergeSortSlices(a[b:])
            return unirProlijo(c, d)
            
mergeSortSlices(lista1)

#5)
def mergeSortIndex(lista):
    """Ordena lista mediante el método merge sort.
       Pre: lista debe contener elementos comparables.
       Devuelve: la lista ordenada."""
   
    def merge_sort_aux(lista, i, j):
        """
        Ordena el segmento de la lista que va
        desde el índice i hasta el j (inclusive).
        Lo hace "inplace".
        """
       
        if j - i == 1:
            if lista[i] > lista[j]:
                lista[i], lista[j] = lista[j], lista[i]
           
        elif j - i >= 2:
            medio = (i + j) // 2
            lista = merge_sort_aux(lista, i, medio-1)
            lista = merge_sort_aux(lista, medio, j)
            lista = merge(lista, i, medio, j)
       
        return lista
   
    return merge_sort_aux(lista, 0, len(lista)-1)


def merge(lista, inicial, mitad, final):
    """
    Hace un merge entre dos segmentos de la lista:
    1) desde el índice inicial hasta mitad-1
    2) desde mitad hasta final (inclusive)
    """
    lista_nueva = []
    i = inicial
    j = mitad
   
    while i < mitad and j <= final:  
        if lista[i] <= lista[j]:    
            lista_nueva.append(lista[i])
            i += 1
        else:
            lista_nueva.append(lista[j])
            j += 1
   
    if i < mitad:
        lista_nueva = lista_nueva + lista[i:mitad]

    else:
        lista_nueva = lista_nueva + lista[j:final+1]
       
    lista[inicial:final+1] = lista_nueva
    return lista
    
    
#6)

def quickSortCopy(a):
    if len(a)==0:
        return []
    if len(a)==1:
        return [a[0]]
    elif len(a)==2:
        if a[0]<a[1]:
            return [a[0], a[1]]
        else:
            return [a[1], a[0]]
    else:
        b=len(a)//2
        m=[]
        M=[]
        i=0
        while i<(len(a)):
            if i==b:
                i=i+1
            else:
                if a[i]<=a[b]:
                    m.append(a[i])
                elif a[i]>a[b]:
                    M.append(a[i])
                i=i+1
        return quickSortCopy(m)+[a[b]]+quickSortCopy(M)

lista1=[6,2,4,9,2,1]
quickSortCopy(lista1)

#7)
#def quickSortIndex(a):
    
lista100=[]
for i in range(0, 100):
    lista100.append(random.randint(0,100))   

lista1000=[]
for i in range(0, 1000):
    lista1000.append(random.randint(0,100))   

lista10000=[]
for i in range(0, 10000):
    lista10000.append(random.randint(0,100))   

lista100000=[]
for i in range(0, 100000):
    lista100000.append(random.randint(0,100))   

#lista1000000=[]
#for i in range(0, 1000000):
#    lista1000000.append(random.randint(0,1000000))   

#lista10000000=[]
#for i in range(0, 10000000):
#    lista10000000.append(random.randint(0,10000000))   

#lista100000000=[]
#for i in range(0, 100000000):
#    lista100000000.append(random.randint(0,100000000))   


#Tiempo upSortSlice
start=time.time()
upSortSlice(lista1000)
end=time.time()
print("Runtime of the program upSortSlice is", end-start, "seconds") 

#Tiempo upSortIndex
start=time.time()
upSortIndex(lista1000)
end=time.time()
print("Runtime of the program upSortIndex is", end-start, "seconds") 

#Tiempo bubbleSort
start=time.time()
bubbleSort(lista1000)
end=time.time()
print("Runtime of the program bubbleSort is", end-start, "seconds") 

#Tiempo mergeSortSlices
start=time.time()
mergeSortSlices(lista1000)
end=time.time()
print("Runtime of the program mergeSortSlices is", end-start, "seconds") 

#Tiempo mergeSortIndex
start=time.time()
mergeSortIndex(lista1000)
end=time.time()
print("Runtime of the program mergeSortIndex is", end-start, "seconds") 

#Tiempo quickSortCopy
start=time.time()
quickSortCopy(lista1000)
end=time.time()
print("Runtime of the program quickSortCopy is", end-start, "seconds") 

#Graficar
start=time.time()
upSortSlice(lista100)
end=time.time()
uss100=end-start 

start=time.time()
upSortSlice(lista1000)
end=time.time()
uss1000=end-start 

start=time.time()
upSortSlice(lista10000)
end=time.time()
uss10000=end-start 


up_sort_slice=[uss100, uss1000]

start=time.time()
upSortIndex(lista100)
end=time.time()
usi100=end-start 

start=time.time()
upSortSlice(lista1000)
end=time.time()
usi1000=end-start 


up_sort_index=[usi100, usi1000]

start=time.time()
bubbleSort(lista100)
end=time.time()
bbs100=end-start 

start=time.time()
bubbleSort(lista1000)
end=time.time()
bbs1000=end-start 


bubble_sort=[bbs100, bbs1000]

start=time.time()
mergeSortSlices(lista100)
end=time.time()
mss100=end-start 

start=time.time()
mergeSortSlices(lista1000)
end=time.time()
mss1000=end-start 


merge_sort_slices=[mss100, mss1000]

start=time.time()
mergeSortIndex(lista100)
end=time.time()
msi100=end-start 

start=time.time()
mergeSortIndex(lista1000)
end=time.time()
msi1000=end-start 


merge_sort_index=[msi100, msi1000]

start=time.time()
quickSortCopy(lista100)
end=time.time()
qsc100=end-start 

start=time.time()
quickSortCopy(lista1000)
end=time.time()
qsc1000=end-start 


quick_sort_copy=[qsc100, qsc1000]

fig = plt.figure(figsize=(10,10))
plt.xscale('log')
plt.plot([100,1000], up_sort_slice, label='Up Sort Slice')
plt.plot([100,1000], up_sort_index, label='Up Sort Index')
plt.plot([100,1000], bubble_sort, label='Bubble Sort')
plt.plot([100,1000], merge_sort_slices, label='Merge Sort Slices')
plt.plot([100,1000], merge_sort_index, label='Merge Sort Index')
plt.plot([100,1000], quick_sort_copy, label='Quick Sort Copy')
plt.title('Tiempos de Ejecución')
plt.xlabel('Tamaño de lista [N]')
plt.ylabel('Tiempo de ejecución [seg]')
plt.legend(loc='best')
plt.savefig('Tiempos de ejecución.pdf')



